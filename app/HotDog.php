<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotDog extends Model
{
    protected $fillable = ['title'];
}
