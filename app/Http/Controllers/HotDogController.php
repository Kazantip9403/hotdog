<?php

namespace App\Http\Controllers;

use App\HotDog;
use Illuminate\Http\Request;

class HotDogController extends Controller
{
    /**
     * @var HotDog
     */
    private $hotDog;

    /**
     * HotDogController constructor.
     * @param HotDog $hotDog
     */
    public function __construct(HotDog $hotDog)
    {
        $this->hotDog = $hotDog;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->hotDog->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $created = $this->hotDog->create([
            'title' => $request->get('title')
        ]);

        return $created;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updated = $this->hotDog->find($id);

        $updated->update([
            'title' => $request->get('title')
        ]);

        return $updated;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->hotDog->find($id)->delete();

        return response('Success', 200);
    }
}
