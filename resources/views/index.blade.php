<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Hot Dogs</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-dark bg-dark">
            <a class="navbar-brand" href="#">
                <img src="{{ asset('images/Hero_Dog.png') }}" alt="" class="logo">HotDog App
            </a>
        </nav>
        <div class="container">
            <hotdog-list />
        </div>
    </div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>